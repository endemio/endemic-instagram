/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

'use strict';

require('../css/app.css');

import Vue from 'vue';
import Vuex from 'vuex';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import Index from '../components/vue/FollowersSearch';
import Spinner from 'vue-spinkit'

Vue.use(BootstrapVue);
Vue.use(Vuex);

// Error console.log
Vue.config.errorHandler = function (err, vm, info)  {
    console.log('[Global Error Handler]: Error in ' + info + ': ' + err);
};

new Vue({
    el: '#followers-search-div',
    components: {Index}
});

Vue.component('Spinner', Spinner);


console.log('Hello Webpack Encore! Edit me in assets/js/followers-search.js');