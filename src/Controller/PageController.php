<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 27.03.2019
 * Time: 0:45
 */

namespace App\Controller;




use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends MainController
{
    /**
     * @Route(path="/",name="index",methods={"GET"})
     * @return Response
     */
    public function index():Response{
        return $this->render('index.html.twig',
            ['timestamp'=>time()]);
    }


    /**
     * @Route(path="/followers-analysis",name="followers_analysis",methods={"GET"})
     * @return Response
     */
    public function pageFollowersSearch():Response{
        return $this->render('followersSearch.html.twig',
            ['timestamp'=>time()]);
    }

    /**
     * @Route(path="/following-analysis",name="following_analysis",methods={"GET"})
     * @return Response
     */
    public function pageFollowingSearch():Response{
        return $this->render('followingsSearch.html.twig',
            ['timestamp'=>time()]);
    }

    /**
     * @Route(path="/analytic",name="analytic",methods={"GET"})
     * @return Response
     */
    public function pageAnalytic():Response{
        return $this->render('analytic.html.twig',
            ['timestamp'=>time()]);
    }

}