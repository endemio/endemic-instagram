<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 05.04.2019
 * Time: 23:13
 */

namespace App\Controller;

use App\Service\ConfigService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class ConfigController extends MainController
{

    /**
     * @Route(path="/api/application/config/load", methods={"POST"})
     * @param Request $request
     * @param ConfigService $configService
     * @return JsonResponse
     */
    public function configLoad(Request $request, ConfigService $configService): JsonResponse
    {

        $name = $request->request->get('project_type');

        try {
            $object = $configService->getConfigDataFromFile();
        } catch (\Exception $exception) {
            throw new HttpException(400, $exception->getMessage());
        }

        if (!empty($object[$name])) {
            return new JsonResponse($object[$name], 200);
        } else {
            return new JsonResponse([], 200);
        }
    }

    /**
     * @Route("/api/application/config/save",methods={"POST"})
     * @param Request $request
     * @param ConfigService $configService
     * @return JsonResponse
     */
    public function saveConfig(Request $request, ConfigService $configService): JsonResponse
    {

        $variables = $request->request->all();

        $name = $variables['project_type'];

        try {
            $object = $configService->getConfigDataFromFile();
        } catch (\Exception $exception) {
            throw new HttpException(400, $exception->getMessage());
        }

        unset($variables['name']);

        $object[$name] = $variables;

        file_put_contents(self::AppConfig, json_encode($object));

        return new JsonResponse(['result' => $object], 200);

    }

}