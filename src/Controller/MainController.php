<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 27.03.2019
 * Time: 0:45
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class MainController extends AbstractController
{

    public const FolderStorage = __DIR__.'/../../storage';

    public const FolderLogging = __DIR__.'/../../logs';

    public const AppConfig = __DIR__.'/../../config/app-config.json';

    public const GoogleCredentials  = __DIR__.'/../../config/abc-instagram-527ba2eba2ee.json';

    public const AWSBucket      = 'followers-bucket';

    public const REDIS_BUCKETS_KEYS = 20000;
}