<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 27.03.2019
 * Time: 6:59
 */

namespace App\Controller;


use App\Service\AccountLocationService;
use App\Service\ElementService;
use App\Service\ProjectService;
use App\Service\AccountService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;

class APIController extends MainController
{

    /**
     * @Route(path="/api/projects/type/{code}/list", name="projects_show", methods={"GET"})
     * @param ProjectService $projectService
     * @param int $code
     * @return JsonResponse
     */
    public function showProjects(int $code,ProjectService $projectService):JsonResponse{

        $projects = $projectService->listAllActiveProjects($code);

        usort($projects, function ($a, $b) {
            $dateA = $a['created'];
            $dateB = $b['created'];
            # use `<=` for descending
            return $dateA <= $dateB;
        });

        return new JsonResponse(['projects'=>$projects,'status'=>true],200);
    }

    /**
     * @Route("/api/project/show-details",methods={"POST"})
     * @param Request $request
     * @param AccountService $accountService
     * @param ElementService $elementService
     * @param ProjectService $projectService
     * @return JsonResponse
     */
    public function projectDetails (
        Request $request,
        AccountService $accountService,
        ElementService $elementService,
        ProjectService $projectService
    ):JsonResponse{

        $id = $request->request->get('project_id');

        try {

            $info = $projectService->getProjectDataById($id);

            $elements = $elementService->getStartElements($info['id']);

            $link=[];
            foreach ($elements as $value){
                $link[$value['username']]=$value;
            }

            $accounts = $accountService->getAccountFullDataByUsernames(array_column($elements,'username'));

            foreach ($accounts as $key=>$account){
                $accounts[$key]['element_id'] = $link[$account['username']]['id'];
                $accounts[$key]['status_following'] = $link[$account['username']]['status_following'];
                $accounts[$key]['status_follower'] = $link[$account['username']]['status_follower'];
            }


        } catch (\Exception $exception){
            throw new HttpException(400,$exception->getMessage());
        }

        return new JsonResponse(['status'=>true,'info'=>$info,'accounts'=>$accounts],200);
    }

    /**
     * @Route("/api/project/show-followings",methods={"POST"})
     * @param Request $request
     * @param AccountService $accountService
     * @param ElementService $elementService
     * @param AccountLocationService $accountLocationService
     * @param ProjectService $projectService
     * @return JsonResponse
     */
    public function getAccountFollowings (
        Request $request,
        AccountService $accountService,
        ElementService $elementService,
        AccountLocationService $accountLocationService,
        ProjectService $projectService
    ):JsonResponse{

        $id = $request->request->get('account_id');

        try {

            $account = $accountService->getAccountEntityById($id);

            $followings = $elementService->getFollowingsFromLocal($account->getUsername());

            $accounts = $accountService->getAccountFullDataByUsernames(array_column($followings,'u'));

            $locations = $accountLocationService->getLocationsByUsernames(array_column($accounts,'username'));

            foreach ($accounts as $key=>$account){
                if (!empty($locations[$account['username']]['result_post'])){
                    $accounts[$key]['location_post']=$locations[$account['username']]['result_post'];
                } else{
                    #unset($accounts[$key]);
                }
            }

        } catch (\Exception $exception){
            throw new HttpException(400,$exception->getMessage());
        }

        return new JsonResponse(['status'=>true,'followings'=>$accounts,'count'=>['followings'=>count($followings)]],200);
    }


    /**
     * @Route("/api/project/show-followers",methods={"POST"})
     * @param Request $request
     * @param AccountService $accountService
     * @param AccountLocationService $accountLocationService
     * @param ElementService $elementService
     * @param ProjectService $projectService
     * @return JsonResponse
     */
    public function getAccountFollowers (
        Request $request,
        AccountService $accountService,
        AccountLocationService $accountLocationService,
        ElementService $elementService,
        ProjectService $projectService
    ):JsonResponse{

        $account_id = $request->request->get('account_id');

        $project_id = $request->request->get('project_id');

        try {

            $account = $accountService->getAccountEntityById($account_id);

            $followers = [];
            if ($project_id) {
                $followers = $projectService->getProjectListFromFileById($project_id,$account->getUsername());
            }

            if (empty($followers)) {
                $list = $elementService->getFollowersFromLocal($account->getUsername() . '-full');
                $followers = array_column($list,'u');
            }

            $accounts = $accountService->getAccountFullDataByUsernames($followers);

            $locations = $accountLocationService->getLocationsByUsernames(array_column($accounts,'username'));

            foreach ($accounts as $key=>$account){
                if (!empty($locations[$account['username']]['result_post'])){
                    $accounts[$key]['location_post']=$locations[$account['username']]['result_post'];
                } else{
                    #unset($accounts[$key]);
                }
            }

        } catch (\Exception $exception){
            throw new HttpException(400,$exception->getMessage());
        }

        return new JsonResponse(['status'=>true,'followers'=>$accounts,'id'=>$account_id,'count_s3'=>count($followers)],200);
    }


}