<?php

namespace App\Repository;

use App\Entity\Element;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Element|null find($id, $lockMode = null, $lockVersion = null)
 * @method Element|null findOneBy(array $criteria, array $orderBy = null)
 * @method Element[]    findAll()
 * @method Element[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ElementRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Element::class);
    }

    // /**
    //  * @return Element[] Returns an array of Element objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Element
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function findElementEntityByProjectId($id){

        $elements = $this->createQueryBuilder('p')
            ->where('p.project_id = :id')
            ->setParameter('id',$id)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($elements as $element){
            $result[] = $this->transform($element);
        }

        return $result;

    }

    public function findElementsToGetFollowers($project_id,$limit,$page){

        $elements = $this->createQueryBuilder('p')
            ->where('p.project_id = :id')
            ->andWhere('p.status_follower = :status')
            ->setParameter('id',$project_id)
            ->setParameter('status',0)
            ->setFirstResult(($page-1)*$limit)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($elements as $element){
            $result[] = $this->transform($element);
        }

        return $result;

    }

    public function findElementsToGetFollowings($project_id,$limit,$page){

        $elements = $this->createQueryBuilder('p')
            ->where('p.project_id = :id')
            ->andWhere('p.status_following = :status')
            ->setParameter('id',$project_id)
            ->setParameter('status',0)
            ->setFirstResult(($page-1)*$limit)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($elements as $element){
            $result[] = $this->transform($element);
        }

        return $result;

    }

    public function findStartElements($project_id,$type){

        $elements = $this->createQueryBuilder('p')
            ->where('p.project_id = :id')
            ->andWhere('p.element_type = :type')
            ->setParameter('id',$project_id)
            ->setParameter('type',$type)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($elements as $element){
            $result[] = $this->transform($element);
        }

        return $result;

    }
    public function findElementsByOwner($master){

        $elements = $this->createQueryBuilder('p')
            ->where('p.owner = :owner')
            ->setParameter('owner',$master)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($elements as $element){
            $result[] = $this->transform($element);
        }

        return $result;

    }
    public function findElementsByOwnerAndUsernames($master,$usernames){

        $elements = $this->createQueryBuilder('p')
            ->where('p.owner = :owner')
            ->andWhere('p.username in (:usernames)')
            ->setParameter('owner',$master)
            ->setParameter('usernames',$usernames)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($elements as $element){
            $result[] = $this->transform($element);
        }

        return $result;

    }

    private function transform(Element $element){
        return [
            'id'=>$element->getId(),
            'username'=>$element->getUsername(),
            'project_id'=>$element->getProjectId(),
            'unique_key'=>$element->getUniqueKey(),
            'owner'=>$element->getOwner(),
            'following'=>$element->getFollowing(),
            'element_type'=>$element->getElementType(),
            'followers'=>$element->getFollowers(),
            'user_real'=>$element->getUsersReal(),
            'user_fake'=>$element->getUsersFake(),
            'user_influ'=>$element->getUsersInflu(),
            'user_inact'=>$element->getUsersInact(),
            'user_mass'=>$element->getUsersMass(),
            'status_follower'=>$element->getStatusFollower(),
            'status_following'=>$element->getStatusFollowing()
        ];
    }

}
