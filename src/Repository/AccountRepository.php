<?php

namespace App\Repository;

use App\Entity\Account;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Account|null find($id, $lockMode = null, $lockVersion = null)
 * @method Account|null findOneBy(array $criteria, array $orderBy = null)
 * @method Account[]    findAll()
 * @method Account[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Account::class);
    }

    // /**
    //  * @return Account[] Returns an array of Account objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Account
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function getAccountWithError($limit){
        $accounts = $this->createQueryBuilder('p')
            ->where('p.error > 0')
            ->andWhere('p.error < 5')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($accounts as $account){
            $result[] = $this->transformAccountByUsername($account);
        }

        return $result;
    }

    public function getAccountEntityByUsername(array $usernames):array {
        $accounts = $this->createQueryBuilder('p')
            ->where('p.username IN (:username)')
            ->setParameter('username',array_values($usernames))
            ->getQuery();

        return $accounts->execute();
    }

    public function findAccountWithFollowersAboveMin($min){
        $accounts = $this->createQueryBuilder('p')
            ->where('p.followers > :min')
            ->andWhere('p.amqp_followers = :status')
            ->setParameter('status',0)
            ->setParameter('min',$min)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($accounts as $account){
            $result[] = $this->transformAccountByUsername($account);
        }

        return $result;
    }

    /**
     * @param array $usernames
     * @return array Account[]
     */
    public function getAccountByUsername(array $usernames):array {
        $accounts = $this->createQueryBuilder('p')
            ->where('p.username IN (:username)')
            ->setParameter('username',array_values($usernames))
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($accounts as $account){
            $result[] = $this->transformAccountByUsername($account);
        }

        return $result;
    }

    private function transformAccountByUsername (Account $account){
        return [
            'username'=>$account->getUsername(),
            'updated'=>$account->getUpdated()
        ];
    }

    /**
     * @param array $usernames
     * @param bool $error
     * @return array
     */
    public function getAccountByUsernameFullData(array $usernames, bool $error):array {
        if ($error) {
            $accounts = $this->createQueryBuilder('p')
                ->where('p.username IN (:username)')
                ->setParameter('username', array_values($usernames))
                ->getQuery()
                ->getResult();
        } else {
            $accounts = $this->createQueryBuilder('p')
                ->where('p.username IN (:username)')
                ->andWhere('p.error < :error')
                ->setParameter('username', array_values($usernames))
                ->setParameter('error', 1)
                ->getQuery()
                ->getResult();
        }

        $result = [];
        foreach ($accounts as $account){
            $result[] = $this->transformAccountByUsernameFullData($account);
        }

        return $result;
    }

    private function transformAccountByUsernameFullData (Account $account){
        return [
            'id'=>$account->getId(),
            'username'=>$account->getUsername(),
            'updated'=>$account->getUpdated(),
            'business'=>$account->getBusiness(),
            'followers' => $account->getFollowers(),
            'follow' => $account->getFollow(),
            'status' => $account->getStatus(),
            'posts' => $account->getPosts(),
            'er' => $account->getEr(),
            'bio' => $account->getBio(),
            'error' => $account->getError(),
            'private'=>$account->getPrivate(),
            'user_id' => $account->getUserId(),
            'public_email'=>$account->getPublicEmail(),
            'category'=>$account->getCategory(),
            'followers_stat'=>$account->getFollowersStat()
        ];
    }    
}
