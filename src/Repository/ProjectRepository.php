<?php

namespace App\Repository;

use App\Entity\Project;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Project|null find($id, $lockMode = null, $lockVersion = null)
 * @method Project|null findOneBy(array $criteria, array $orderBy = null)
 * @method Project[]    findAll()
 * @method Project[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProjectRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Project::class);
    }

    // /**
    //  * @return Project[] Returns an array of Project objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Project
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllActiveProjectsByType(int $type){
        $list = $this->createQueryBuilder('p')
            ->where('p.status = :status')
            ->andWhere('p.project_type = :type')
            ->setParameter('status',1)
            ->setParameter('type',$type)
            ->getQuery()
            ->getResult();

        $result = [];
        foreach ($list as $item){
            $result[] = $this->transform($item);
        }

        return $result;
    }

    /**
     * @param $id
     * @return array
     * @throws \Exception
     */
    public function getInfoById($id){
        try {
            $result = $this->createQueryBuilder('p')
                ->where('p.id = :id')
                ->setParameter('id', $id)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();

        } catch (NonUniqueResultException $exception) {
            throw new \Exception($exception->getMessage());
        } catch (\Exception $exception){
            throw new \Exception($exception->getMessage());
        }

        if (!empty($result)) {
            return $this->transform($result);
        } else {
            return [];
        }
    }

    private function transform(Project $project){
        return [
            'id'=>$project->getId(),
            'start'=>$project->getStart(),
            'title'=>$project->getTitle(),
            'status'=>$project->getStatus(),
            'created'=>$project->getCreated(),
            'updated'=>$project->getUpdated(),
            'project_type'=>$project->getProjectType(),
            'detail'=>$project->getDetail()
        ];
    }


}
