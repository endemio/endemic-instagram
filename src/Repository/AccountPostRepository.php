<?php

namespace App\Repository;

use App\Entity\AccountPost;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AccountPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountPost[]    findAll()
 * @method AccountPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountPostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AccountPost::class);
    }

    // /**
    //  * @return AccountPost[] Returns an array of AccountPost objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AccountPost
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findPostsExistsByUsername(array $usernames){

        $elements = $this->createQueryBuilder('p')
            ->where('p.username IN (:usernames)')
            ->setParameter('usernames',$usernames)
            ->getQuery()
            ->getResult();

        $result = [];

        foreach ($elements as $element){
            $temp = $this->transform($element);

            if (!empty($temp['posts'])){
                array_push($result,$temp['username']);
            }

        }

        return $result;
    }

    private function transform(AccountPost $accountPost){

        return [
            'username'=>$accountPost->getUsername(),
            'posts'=>$accountPost->getPost(),
            'updated'=>$accountPost->getUpdated()
        ];

    }
}
