<?php

namespace App\Repository;

use App\Entity\AccountLocation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AccountLocation|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountLocation|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountLocation[]    findAll()
 * @method AccountLocation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountLocationRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AccountLocation::class);
    }

    // /**
    //  * @return AccountLocation[] Returns an array of AccountLocation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AccountLocation
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findLocationsByUsername(array $username){

        $locations = $this->createQueryBuilder('p')
            ->where('p.username IN (:usernames)')
            ->setParameter('usernames',$username)
            ->getQuery()
            ->getResult();

        $result = [];

        foreach ($locations as $location){
            $temp = $this->transform($location);
            $result[$temp['username']] = $temp;
        }

        return $result;
    }

    private function transform(AccountLocation $location){
        return [
            'username'=>$location->getUsername(),
            'location_bio'=>$location->getLocationBio(),
            'location_post'=>$location->getLocationPost(),
            'update'=>$location->getUpdated(),
            'result_bio'=>$location->getResultBio(),
            'result_post'=>$location->getResultPost(),
            'summary'=>$location->getSummary()
            ];
    }

}
