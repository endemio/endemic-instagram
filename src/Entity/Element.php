<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ElementRepository")
 */
class Element
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $username;

    /**
     * @ORM\Column(type="integer")
     */
    private $project_id;

    /**
     * @ORM\Column(type="string", length=100,unique=true)
     */
    private $unique_key;

    /**
     * @ORM\Column(type="integer")
     */
    private $element_type;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $owner;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $following;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $users_real;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $users_fake;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $users_influ;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $users_inact;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $users_mass;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $followers;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status_follower;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status_following;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $followings;

    public function __construct() {
        $this->published = false;
        $this->followers = false;
        $this->users_fake = 0;
        $this->users_inact = 0;
        $this->users_influ = 0;
        $this->users_mass = 0;
        $this->users_real = 0;
        $this->status_follower = 0;
        $this->status_following = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getProjectId(): ?int
    {
        return $this->project_id;
    }

    public function setProjectId(int $project_id): self
    {
        $this->project_id = $project_id;

        return $this;
    }

    public function getUniqueKey(): ?string
    {
        return $this->unique_key;
    }

    public function setUniqueKey(string $unique_key): self
    {
        $this->unique_key = $unique_key;

        return $this;
    }

    public function getElementType(): ?int
    {
        return $this->element_type;
    }

    public function setElementType(int $element_type): self
    {
        $this->element_type = $element_type;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getOwner(): ?string
    {
        return $this->owner;
    }

    public function setOwner(?string $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getFollowing(): ?bool
    {
        return $this->following;
    }

    public function setFollowing(?bool $following): self
    {
        $this->following = $following;

        return $this;
    }

    public function getUsersReal(): ?float
    {
        return $this->users_real;
    }

    public function setUsersReal(?float $users_real): self
    {
        $this->users_real = $users_real;

        return $this;
    }

    public function getUsersFake(): ?float
    {
        return $this->users_fake;
    }

    public function setUsersFake(?float $users_fake): self
    {
        $this->users_fake = $users_fake;

        return $this;
    }

    public function getUsersInflu(): ?float
    {
        return $this->users_influ;
    }

    public function setUsersInflu(?float $users_influ): self
    {
        $this->users_influ = $users_influ;

        return $this;
    }

    public function getUsersInact(): ?float
    {
        return $this->users_inact;
    }

    public function setUsersInact(?float $users_inact): self
    {
        $this->users_inact = $users_inact;

        return $this;
    }

    public function getUsersMass(): ?float
    {
        return $this->users_mass;
    }

    public function setUsersMass(?float $users_mass): self
    {
        $this->users_mass = $users_mass;

        return $this;
    }

    public function getFollowers(): ?int
    {
        return $this->followers;
    }

    public function setFollowers(?int $followers): self
    {
        $this->followers = $followers;

        return $this;
    }

    public function getStatusFollower(): ?int
    {
        return $this->status_follower;
    }

    public function setStatusFollower(?int $status_follower): self
    {
        $this->status_follower = $status_follower;

        return $this;
    }

    public function getStatusFollowing(): ?int
    {
        return $this->status_following;
    }

    public function setStatusFollowing(?int $status_following): self
    {
        $this->status_following = $status_following;

        return $this;
    }

    public function getFollowings(): ?int
    {
        return $this->followings;
    }

    public function setFollowings(?int $followings): self
    {
        $this->followings = $followings;

        return $this;
    }
}
