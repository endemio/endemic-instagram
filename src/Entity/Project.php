<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Datetime;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="integer")
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated;

    /**
     * @ORM\Column(type="boolean")
     */
    private $start;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $detail;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $project_type;

    /*
     * Custom __construct to set default
     */
    public function __construct() {
        $this->created = new DateTime();
        $this->updated = DateTime::createFromFormat("U", 1);
        $this->status = 1;
        $this->start = false;
        $this->project_type = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreated(): ?Datetime
    {
        return $this->created;
    }

    public function setCreated(int $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getUpdated(): ?Datetime
    {
        return $this->updated;
    }

    public function setUpdated(DateTime $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getStart(): ?bool
    {
        return $this->start;
    }

    public function setStart(bool $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getDetail()
    {
        return $this->detail;
    }

    public function setDetail($detail): self
    {
        $this->detail = $detail;

        return $this;
    }

    public function getProjectType(): ?int
    {
        return $this->project_type;
    }

    public function setProjectType(?int $project_type): self
    {
        $this->project_type = $project_type;

        return $this;
    }
}
