<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountRepository")
 */
class Account
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=30, unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $follow;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $followers;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $posts;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $er;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $bio;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $user_id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $added;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $business;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $error;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $private;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $public_email;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $bio_email;

    /**
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $followers_stat;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $category;

    public function __construct(){
        $this->error = 1;
        $this->er = 0.0;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getFollow(): ?int
    {
        return $this->follow;
    }

    public function setFollow(int $follow): self
    {
        $this->follow = $follow;

        return $this;
    }

    public function getFollowers(): ?int
    {
        return $this->followers;
    }

    public function setFollowers(int $followers): self
    {
        $this->followers = $followers;

        return $this;
    }

    public function getPosts(): ?int
    {
        return $this->posts;
    }

    public function setPosts(int $posts): self
    {
        $this->posts = $posts;

        return $this;
    }

    public function getEr(): ?float
    {
        return $this->er;
    }

    public function setEr(float $er): self
    {
        $this->er = $er;

        return $this;
    }

    public function getBio(): ?string
    {
        return $this->bio;
    }

    public function setBio(string $bio): self
    {
        $this->bio = $bio;

        return $this;
    }

    public function getUserId(): ?int
    {
        return $this->user_id;
    }

    public function setUserId(int $user_id): self
    {
        $this->user_id = $user_id;

        return $this;
    }

    public function getAdded(): ?int
    {
        return $this->added;
    }

    public function setAdded(int $added): self
    {
        $this->added = $added;

        return $this;
    }

    public function getUpdated(): ?\DateTime
    {
        return $this->updated;
    }

    public function setUpdated(\DateTime $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getBusiness(): ?int
    {
        return $this->business;
    }

    public function setBusiness(int $business): self
    {
        $this->business = $business;

        return $this;
    }

    public function getError(): ?int
    {
        return $this->error;
    }

    public function setError(int $error): self
    {
        $this->error = $error;

        return $this;
    }

    public function getPrivate(): ?bool
    {
        return $this->private;
    }

    public function setPrivate(bool $private): self
    {
        $this->private = $private;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getPublicEmail(): ?string
    {
        return $this->public_email;
    }

    public function setPublicEmail(?string $public_email): self
    {
        $this->public_email = $public_email;

        return $this;
    }

    public function getBioEmail()
    {
        return $this->bio_email;
    }

    public function setBioEmail($bio_email): self
    {
        $this->bio_email = $bio_email;

        return $this;
    }

    public function getJsonPosts()
    {
        return $this->json_posts;
    }

    public function setJsonPosts($json_posts): self
    {
        $this->json_posts = $json_posts;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getFollowersStat()
    {
        return $this->followers_stat;
    }

    public function setFollowersStat($followers_stat): self
    {
        $this->followers_stat = $followers_stat;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(?string $category): self
    {
        $this->category = $category;

        return $this;
    }
}
