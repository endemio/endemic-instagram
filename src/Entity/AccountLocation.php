<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AccountLocationRepository")
 */
class AccountLocation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40,unique=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $location_bio;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $location_post;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $result_bio;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $result_post;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $main_location_country;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $main_location_city;

    /**
     * @ORM\Column(type="json_array", nullable=true)
     */
    private $summary;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getLocationBio()
    {
        return $this->location_bio;
    }

    public function setLocationBio($location_bio): self
    {
        $this->location_bio = $location_bio;

        return $this;
    }

    public function getLocationPost()
    {
        return $this->location_post;
    }

    public function setLocationPost($location_post): self
    {
        $this->location_post = $location_post;

        return $this;
    }

    public function getUpdated(): ?\DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(?\DateTimeInterface $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getResultBio()
    {
        return $this->result_bio;
    }

    public function setResultBio($result_bio): self
    {
        $this->result_bio = $result_bio;

        return $this;
    }

    public function getResultPost()
    {
        return $this->result_post;
    }

    public function setResultPost($result_post): self
    {
        $this->result_post = $result_post;

        return $this;
    }

    public function getMainLocationCountry(): ?string
    {
        return $this->main_location_country;
    }

    public function setMainLocationCountry(?string $main_location_country): self
    {
        $this->main_location_country = $main_location_country;

        return $this;
    }

    public function getMainLocationCity(): ?string
    {
        return $this->main_location_city;
    }

    public function setMainLocationCity(?string $main_location_city): self
    {
        $this->main_location_city = $main_location_city;

        return $this;
    }

    public function getSummary()
    {
        return $this->summary;
    }

    public function setSummary($summary): self
    {
        $this->summary = $summary;

        return $this;
    }
}
