<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 28.03.2019
 * Time: 10:22
 */

namespace App\Service;

use App\Controller\MainController;
use App\Entity\AccountLocation;
use DateTime;

class AccountLocationService extends MainController
{

    public function addLocationsByUsername($data){

        $em = $this->getDoctrine()->getManager();

        foreach ($data as $item){

            $entity = $em->getRepository(AccountLocation::class)->findOneBy(['username'=>$item['username']]);

            if ($entity == null) {
                $entity = new AccountLocation();
            }

            $entity = $this->updateAccountLocationEntity($entity,$item);

            $em->persist($entity);
        }

        $em->flush();

        $em->clear();
    }

    private function updateAccountLocationEntity(AccountLocation $entity,$data){

        $updated = new DateTime();

        $entity
            ->setUsername(!empty($data['username'])?$data['username']:$entity->getUsername())
            ->setUpdated($updated)
            ->setResultPost(isset($data['result_posts'])?$data['result_posts']:$entity->getResultPost())
            ->setResultBio(isset($data['result_bio'])?$data['result_bio']:$entity->getResultBio())
            ->setMainLocationCountry(isset($data['main_country'])?$data['main_country']:$entity->getMainLocationCountry())
            ->setMainLocationCity(isset($data['main_city'])?$data['main_city']:$entity->getMainLocationCity())
            ->setLocationBio(!empty($data['location_bio'])?$data['location_bio']:$entity->getLocationBio())
            ->setSummary(isset($data['summary'])?$data['summary']:$entity->getSummary())
            ->setLocationPost(!empty($data['location_post'])?$data['location_post']:$entity->getLocationPost());

        return $entity;
    }

    public function updateResultPostByArray(string $username, array $locations){

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(AccountLocation::class)->findOneBy(['username'=>$username]);

        if ($entity == null) {
            return false;
        }

        $entity->setResultPost($locations);
        $em->persist($entity);
        $em->flush();
        $em->clear();

        return true;
    }

    public function updateSummaryLocationByArray(string $username, array $summary){

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(AccountLocation::class)->findOneBy(['username'=>$username]);

        if ($entity == null) {
            return false;
        }

        $entity->setSummary($summary);
        $em->persist($entity);
        $em->flush();
        $em->clear();

        return true;
    }



    public function updateMainLocationNyUsername(string $username, array $data){

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(AccountLocation::class)->findOneBy(['username'=>$username]);

        $entity = $this->updateAccountLocationEntity($entity,$data);

        $em->persist($entity);

        $em->flush();

        $em->clear();
    }


    /**
     * @param string $username
     * @return AccountLocation|null|object
     */
    public function getLocationEntityByUsername(string $username){

        $entity = $this->getDoctrine()->getRepository(AccountLocation::class)->findOneBy(['username'=>$username]);

        return $entity;
    }

    /**
     * @param array $usernames
     * @return array
     */
    public function getLocationsByUsernames(array $usernames){

        $locations = $this->getDoctrine()->getRepository(AccountLocation::class)->findLocationsByUsername($usernames);

        return $locations;
    }


    /**
     * @param array $usernames
     * @return array
     */
    public function analyzeAccountLocationsByUsernames(array $usernames){

        $locations = $this->getLocationsByUsernames($usernames);

        $result =[];
        foreach ($locations as $key=>$item){
            $result[$key] = $this->analyzeAccountLocationsResultPost($item['result_post']);
        }

        return $result;

    }

    /**
     * @param array $result_post
     * @return array
     */
    public function analyzeAccountLocationsResultPost(array $result_post){

        $countries = [];
        if (!empty($result_post)){
            foreach ($result_post as $location){
                $full_name = \Locale::getDisplayRegion('-'.$location['country'],'en');
                if (empty($countries[$full_name])) $countries[$full_name]=1;
                else $countries[$full_name]++;
            }
        }

        $percent = [];

        foreach ($countries as $country=>$value){
            if (array_sum($countries) > 0) {
                $percent[$country] = number_format($value / array_sum($countries) * 100, 0);
            }
        }

        $string = [];
        $items = [];
        if (!empty($percent)){
            foreach ($percent as $key=>$item){
                $string[]=!empty($key)?$key:'Not Found'.' '.$item.'%';
                $items[!empty($key)?$key:'NotFound']=$item/100;

            }

            return [
                'main_country'=>array_keys($percent, max($percent))[0],
                'main_percent'=>$percent[array_keys($percent, max($percent))[0]],
                'all_country'=>implode($string,', '),
                'all_country_array'=>$items,
                'main_city'=>''
            ];
        }

        return [];

    }
}