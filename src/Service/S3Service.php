<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 13.03.2019
 * Time: 2:42
 */

namespace App\Service;


use App\Controller\MainController;
use Aws\S3\Exception\S3Exception;
use Aws\Sdk;


class S3Service extends MainController {

    protected $sdk;

    public function __construct(Sdk $sdk){
        $this->sdk = $sdk;
    }

    /**
     * @param $username
     * @param $append
     * @param $bucket
     * @return array
     */
    public function getAccountsListByOwnerUsername($username, $append, $bucket){

        $result = [];

        try {
            $file = $this->sdk->createS3()->getObject(['Bucket' => $bucket, 'Key' => $username.$append.'.dict']);

            $array = explode(PHP_EOL, $file['Body']);

            foreach ($array as $item){
                $result[] = json_decode(str_replace("'",'"',$item),true);
            }

            return $result;
        } catch (S3Exception $e) {
            return [];
        }
    }

    /**
     * @param $username
     * @param $append
     * @param $bucket
     * @return array
     */
    public function getJSONArrayByUsername($username, $append, $bucket){

        $result = [];

        try {
            $file = $this->sdk->createS3()->getObject(['Bucket' => $bucket, 'Key' => $username.$append.'.json']);

            $result[] = json_decode($file['Body']);

            return $result;
        } catch (S3Exception $e) {
            return [];
        }
    }

    /**
     * @param $filename
     * @param $bucket
     * @return bool
     * @throws \Exception
     */
    public function checkIfFileExists($filename,$bucket){

        try {
            $response = $this->sdk->createS3()->doesObjectExist($bucket, $filename);
            return $response;
        } catch (S3Exception $e) {
            throw new \Exception($e->getMessage());
        }


    }

}