<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 22.12.2018
 * Time: 12:46
 */

namespace App\Service;

use Symfony\Component\HttpKernel\Exception\HttpException;

class EmailService {

    private $mailer;

    private $twig;

    public function __construct(\Swift_Mailer $mailer, \Twig_Environment $twig) {
        $this->mailer = $mailer;

        $this->twig = $twig;
    }

    public function sendContactMail($template,$sender,$recipients,$subject,$data){

        try {
            $body =  $this->twig->render(
                $template,
                $data
            ) ;
        } catch (\Exception $exception){
            throw new HttpException(400,$exception->getMessage());
        }

        $message = (new \Swift_Message($subject))
            ->setFrom($sender)
            ->setTo($recipients)
            ->setBody(
                $body,
                'text/html'
            )
        ;
        $this->mailer->send($message);
    }
}