<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 13.03.2019
 * Time: 0:08
 */

namespace App\Service;

use App\Controller\MainController;
use Predis\ClientInterface;

class RedisService extends MainController
{

    /**
     * @var ClientInterface
     */
    private $redisClient;


    public function __construct(ClientInterface $client){

        $this->redisClient = $client;
    }


    public function getValueByKey($key,$index){

        $value = $this->redisClient->hget($key,$index);

        return $value;

    }

    public function getValueByUserId($user_id){

        $bucket_index = (int)($user_id/self::REDIS_BUCKETS_KEYS);
        $index = $user_id - $bucket_index*self::REDIS_BUCKETS_KEYS;

        $value = $this->getValueByKey($bucket_index,$index);

        return $value;

    }



}