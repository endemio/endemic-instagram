<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 27.03.2019
 * Time: 10:47
 */

namespace App\Service;


use App\Controller\MainController;
use App\Entity\Element;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;

class ElementService extends MainController
{

    private $s3;

    public function __construct(S3Service $s3Service){

        $this->s3 = $s3Service;
    }

     public function getAccountsToGetFollowers(int $project_id,$limit,$page){

        $list = $this->getDoctrine()->getRepository(Element::class)->findElementsToGetFollowers($project_id,$limit,$page);

        return $list;

    }

    public function getAccountsToGetFollowings(int $project_id,$limit,$page){

        $list = $this->getDoctrine()->getRepository(Element::class)->findElementsToGetFollowings($project_id,$limit,$page);

        return $list;

    }

    public function getElementEntityByUsername($username,$id){

        $result = $this->getDoctrine()->getRepository(Element::class)->findOneBy(
            ['username'=>$username,'project_id'=>$id]);

        return $result;

    }

    /**
     * @param int $project_id
     * @param array $data
     * @throws Exception
     */
    public function updateElementsByProjectId(int $project_id, array $data){

        $em = $this->getDoctrine()->getManager();

        $elements =$em->getRepository(Element::class)->findBy(['project_id'=>$project_id]);

        foreach ($elements as $element){
            try{
                $element = $this->updateElementEntity($element,$data);
                $em->persist($element);
            } catch (Exception $exception){
                throw  new Exception($exception->getMessage());
            }
        }

        $em->flush();

        $em->clear();
    }

    /**
     * @param array $ids
     * @param array $data
     */
    public function updateElementsByIds(array $ids, array $data){

        $em = $this->getDoctrine()->getManager();

        foreach ($ids as $id) {

            $element = $em->getRepository(Element::class)->find($id);

            $element = $this->updateElementEntity($element,$data);

            $em->persist($element);
        }

        $em->flush();

        $em->clear();
    }

    public function getStartElements($project_id){

        $list = $this->getDoctrine()->getRepository(Element::class)->findStartElements($project_id,1);

        return $list;

    }

    public function getElementsByOwnerAndUsernames($master,$usernames){

        $list = $this->getDoctrine()->getRepository(Element::class)->findElementsByOwnerAndUsernames($master,$usernames);

        return $list;

    }

    public function getElementEntityById(int $id){

        $element = $this->getDoctrine()->getRepository(Element::class)->find($id);

        return $element;
    }

    public function getElementEntityByUniqueKey(string $unique){
        $element = $this->getDoctrine()->getRepository(Element::class)->findOneBy(['unique_key'=>$unique]);

        return $element;

    }

    public function getElementEntityByProjectId(int $project_id){

        $list = $this->getDoctrine()->getRepository(Element::class)->findElementEntityByProjectId($project_id);

        return $list;

    }

    public function getElementsByOwner($master){

        $list = $this->getDoctrine()->getRepository(Element::class)->findElementsByOwner($master);

        return $list;

    }

    /**
     * @param $username
     * @return array
     * @throws Exception
     */
    public function getFollowersListFromS3($username,$append){

        $result =[];

        $localfile = self::FolderStorage.'/followers/'.$username.$append.'.dict';

        if (is_file($localfile)){
            $list = json_decode(file_get_contents($localfile));
        } else {
            try {
                $list = $this->s3->getAccountsListByOwnerUsername($username, $append, 'followers-bucket');
                file_put_contents($localfile,json_encode($list));
            } catch (Exception $exception) {
                throw new Exception($exception->getMessage());
            }
        }

        foreach ($list as $item) {
            if (!empty($item->c)) {
                array_push($result, $item);
            }
        }

        return $result;
    }

    /**
     * @param $unique
     * @param $data
     */
    public function updateElementsByUniqueKey($unique,$data){

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(Element::class)->findOneBy(['unique_key'=>$unique]);

        if ($entity) {

            $this->updateElementEntity($entity,$data);

            $em->persist($entity);
            $em->flush();
            $em->clear();
        }
    }

    /**
     * @param $username
     * @return array|mixed
     * @throws Exception
     */
    public function getFollowingsFromLocal($username){

        # Store file from s3 to local storage
        $filename = self::FolderStorage . '/followings/' . $username . '-full.json';

        if (!is_file($filename)) {
            try {
                $followings = $this->s3->getAccountsListByOwnerUsername($username, '', 'followings-bucket');
            } catch (Exception $exception){
                throw new Exception($exception->getMessage());
            }
            file_put_contents($filename, json_encode($followings));
        } else {
            $followings = json_decode(file_get_contents($filename), true);
        }

        return $followings;
    }

    /**
     * @param $username
     * @return array|mixed
     * @throws Exception
     */
    public function getFollowersFromLocal($username){

        # Store file from s3 to local storage
        $filename = self::FolderStorage . '/followers/' . $username . '.json';

        if (!is_file($filename)) {

            try {
                $followers = $this->s3->getAccountsListByOwnerUsername($username, '', 'followers-bucket');
            } catch (Exception $exception){
                throw new Exception($exception->getMessage());
            }

            file_put_contents($filename, json_encode($followers));
        } else {

            $content = file_get_contents($filename);

            if (strlen($content)<1){
                try {
                    $followers = $this->s3->getAccountsListByOwnerUsername($username, '', 'followers-bucket');
                } catch (Exception $exception){
                    throw new Exception($exception->getMessage());
                }
                file_put_contents($filename, json_encode($followers));
            } else {
                $followers = json_decode(file_get_contents($filename), true);
            }
        }

        return $followers;
    }

    public function storeFollowersToLocal($username,$followers){

        # Store file from s3 to local storage
        $filename = self::FolderStorage . '/followers/' . $username . '.json';

        file_put_contents($filename, json_encode($followers));

        return true;
    }


    public function removeFollowersFileFromLocal($username){

        # Remove
        $filename = self::FolderStorage . '/followers/' . $username . '.json';

        if (is_file($filename)) {
            unlink($filename);
            return true;
        }

    }

    /**
     * @param Element $element
     * @param array $data
     * @return Element
     */
    private function updateElementEntity(Element $element, array $data){

        $element
            ->setUniqueKey(!empty($data['unique_key']) ? $data['unique_key'] : $element->getUniqueKey())
            ->setUsersFake(!empty($data['fake']) ? $data['fake'] : $element->getUsersFake())
            ->setUsersInflu(!empty($data['influ']) ? $data['influ'] : $element->getUsersInflu())
            ->setUsersInact(!empty($data['inact']) ? $data['inact'] : $element->getUsersInact())
            ->setUsersMass(!empty($data['mass']) ? $data['mass'] : $element->getUsersMass())
            ->setUsersReal(!empty($data['real']) ? $data['real'] : $element->getUsersReal())
            ->setStatusFollower(isset($data['status_follower']) ? $data['status_follower'] : $element->getStatusFollower())
            ->setStatusFollowing(isset($data['status_following']) ? $data['status_following'] : $element->getStatusFollowing());

        return $element;
    }

}