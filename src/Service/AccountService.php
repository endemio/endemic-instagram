<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 04.01.2019
 * Time: 0:08
 */

namespace App\Service;

use App\Controller\MainController;
use App\Entity\Account;


class AccountService extends MainController {


    public function updateAccountDataByUsername($username,$data){

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(Account::class)->findOneBy(['username'=>$username]);

        if (!empty($entity)) {
            $entity
                ->setUpdated(new \DateTime())
                ->setPrivate(isset($data['private']) ? $data['private'] : $entity->getPrivate())
                ->setFollow(isset($data['follow']) ? $data['follow'] : $entity->getFollow())
                ->setUserId(isset($data['user_id']) ? $data['user_id'] : $entity->getUserId())
                ->setFollowers(isset($data['followers']) ? $data['followers'] : $entity->getFollowers())
                ->setEr(isset($data['er']) ? $data['er'] : (!empty($entity->getEr()?$entity->getEr():0)))
                ->setPosts(isset($data['posts_count']) ? $data['posts_count'] : $entity->getPosts())
                ->setBusiness(isset($data['business']) ? $data['business'] : $entity->getBusiness())
                ->setError(isset($data['error']) ? $data['error'] : $entity->getError())
                ->setFollowersStat(isset($data['followers_stat']) ? $data['followers_stat'] : $entity->getError())
                ->setBio(isset($data['bio']) ? $data['bio'] : $entity->getBio());

            $em->flush();

            $em->clear();
        }
    }

    public function updateAccountInfoByUsername($username,$data){

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository(Account::class)->findOneBy(['username'=>$username]);

        $emails =[];
        if (!empty($data['biography'])) {
            $emails = $this->parseEmailsFromString($data['biography']);
        }

        $entity
            ->setBioEmail($emails)
            ->setPublicEmail(isset($data['public_email'])?$data['public_email']:(!empty($entity->getPublicEmail())?$entity->getPublicEmail():''))
            ->setCity(isset($data['city_name'])?$data['city_name']:(!empty($entity->getCity())?$entity->getCity():''));

        #$print_r($data);

        $em->flush();

        $em->clear();
    }


    /**
     * @param array $usernames
     * @param bool $error
     * @return array
     */
    public function getAccountFullDataByUsernames (array $usernames, bool $error = true){

        $list = $this->getDoctrine()->getRepository(Account::class)->getAccountByUsernameFullData($usernames,$error);

        foreach ($list as $key=>$item){
            $list[$key]['bio_email'] = $this->parseEmailsFromString($item['bio']);
        }

        return $list;
    }

    public function getAccountDataWithError($limit){
        $list = $this->getDoctrine()->getRepository(Account::class)->getAccountWithError($limit);

        return $list;
    }



    public function getAccountEntityById(int $id){
        $account = $this->getDoctrine()->getRepository(Account::class)->find($id);
        return $account;
    }

    public function getAccountWithFollowersAboveMin($project_id,$min){
        $list = $this->getDoctrine()->getRepository(Account::class)->findAccountWithFollowersAboveMin($min);
        return $list;

    }


    /**
     * @param array $usernames
     * @return bool
     */
    public function addAccounts(array $usernames){

        $em = $this->getDoctrine()->getManager();

        foreach ($usernames as $username){

            $account = new Account();

            $account->setUsername($username);

            $em->persist($account);

        }

        $em->flush();

        $em->clear();

        return true;
    }


    private function parseEmailsFromString($string){
        preg_match_all("/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i", $string, $matches);

        return (!empty($matches[0]))?implode(',',$matches[0]):'';
    }


    public function checkAccount($value, $follower){
        if (!empty($value[0])) {
            $type = $this->checkAccountTypeByValues([
                'posts' => $value[2],
                'follow' => $value[3],
                'followers' => $value[4],
                'business' => $value[5],
                'private' => $value[6]]);

        } else {
            $type['summary'] = [0,0,0,0,0];
        }
        return $type;
    }


    public function checkMultipleAccountsForAnalyze($followers){

        $result = [];

        foreach ($followers as $key=>$follower){

            $value = explode(':',str_replace('|',':',$follower));

            try {
                if (!empty($value[0])) {
                    $result[$key] = $this->checkAccountTypeByValues([
                        'posts' => $value[2],
                        'follow' => $value[3],
                        'followers' => $value[4],
                        'business' => $value[5],
                        'private' => $value[6]]);

                } else {
                    $result[$key] = ['summary' => [0, 0, 0, 0, 0]];
                }
            } catch (\Exception $exception){
                throw new \Exception(__FUNCTION__.' '.$exception->getMessage().' '.json_encode($value));
            }
        }

        return $result;
    }


    public function checkAccountTypeByValues($values){

        /*
                        #Followers	    #Follow 	    #Post
        Real people	    20<x<1000	    20<x<1000	    x > 12
        Influencers	    x > 1000        20<x<1000	    x > 24
        Fake	        x > 1000	    x > 1000        N.A.
        Inactive	    x < 20	        x < 1000	    N.A.
        Mass Followers	x < 1000	    x > 1000	    N.A.
        Suspicious	    Rest	        Rest	        Rest
         */

        $posts = [
            'real'  => ($values['posts'] > 0)?(int)$values['posts']:1/100,
            'influ' => ($values['posts'] > 1)?$values['posts']/12:0,
            'fake'  => ($values['posts'] < 20)?(($values['posts'] > 0)?10/$values['posts']:100):1/20,
            'inact' => ($values['posts'] < 10)?(($values['posts'] > 0)?10/$values['posts']:100):0,
            'mass'  => ($values['posts'] > 0)?1:1,
        ];

        $follow = [
            'real'  => ($values['follow'] < 1000)?(100/($values['follow']+1)):0,
            'influ' => ($values['follow'] < 1000)?(100/($values['follow']+1)):0,
            'fake'  => ($values['follow'] > 100)?($values['follow']/100):0,
            'inact' => ($values['follow'] > 0)?(100/$values['follow']):0,
            'mass'  => ($values['follow'] > 100)?($values['follow']/100):0,
        ];

        $followers = [
            'real'  => ($values['followers'] > 1 && $values['followers'] < 1000)?($values['followers']/6):0,
            'influ' => ($values['followers'] > 1000)?($values['followers']/100):0,
            'fake'  => ($values['followers'] > 1)?(10/$values['followers']):100,
            'inact' => ($values['followers'] > 0)?(100/($values['followers']+1)):100,
            'mass'  => ($values['followers'] > 1)?($values['followers']/1000):0,
        ];

        $private = [
            'real'  => ($values['private'])?1/3:10,
            'influ' => ($values['private'])?0:10,
            'fake'  => ($values['private'])?5:1,
            'inact' => ($values['private'])?1:1,
            'mass'  => ($values['private'])?1:1,
        ];

        $business = [
            'real'  => ($values['business'])?1/10:1,
            'influ' => ($values['business'])?20:1,
            'fake'  => ($values['business'])?0:1,
            'inact' => ($values['business'])?0:1,
            'mass'  => ($values['business'])?1:1,
        ];

        $sum = [];
        foreach (array_keys($posts) as $key){
            $sum[$key] = $posts[$key]*$follow[$key]*$followers[$key]*$private[$key]*$business[$key];
        }

        $summary = array_sum($sum);
        foreach ($sum as $key=>$value){
            if ($summary > 0) {
                $sum[$key] = number_format($value*100/$summary, 1);
            }
        }

        return [
            'input'=>[
                'posts'=>$values['posts'],
                'follow'=>$values['follow'],
                'followers'=>$values['followers'],
                'private'=>$values['private'],
                'business'=>$values['business']
            ],
            'summary'=>$sum,
            'details'=>[
                'posts'=>$posts,
                'follow'=>$follow,
                'followers'=>$followers,
                'private'=>$private,
                'business'=>$business]
        ];

    }

}