<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 04.01.2019
 * Time: 22:40
 */

namespace App\Service;

use App\Controller\MainController;
use Google_Client;
use Google_Service_Sheets;
use Exception;

class SpreadsheetService extends MainController {

    private $elementService;

    private $accountService;

    private $accountLocationsService;

    public function __construct(
        ElementService $elementService,
        AccountLocationService $accountLocationService,
        AccountService $accountService
    ){
        $this->elementService = $elementService;

        $this->accountService = $accountService;

        $this->accountLocationsService = $accountLocationService;
    }

    /**
     * @param $username
     * @param $folderId
     * @return string
     * @throws Exception
     */
    public function exportFollowingsByUsernameToSpreadsheet ($username, $usernames, $folderId, $check_locations = false) {

        $accounts = $this->accountService->getAccountFullDataByUsernames($usernames);

        if ($check_locations) {

            $analyze = $this->accountLocationsService->analyzeAccountLocationsByUsernames(array_column($accounts, 'username'));

            foreach ($accounts as $key => $account) {
                if (!empty($analyze[$account['username']])) {
                    $accounts[$key] = array_merge($accounts[$key], $analyze[$account['username']]);
                } else {
                    $accounts[$key] = array_merge($accounts[$key], [
                        'main_country' => '',
                        'main_percent' => 0,
                        'all_country' => ''
                    ]);
                }
            }
        }

        // Export to sheets

        $spreadsheetId = $this->createFileInFolderIfNotExistOrReturnSpreadsheetId($folderId,$username);

        $header = $this->generateSheetAccountsWithLocationsFirstRow();

        $list=[];
        foreach ($accounts as $account){
            array_push($list,$this->generateSheetAccountsWithLocationsDataRow($account));
        }

        $this->sendInstagramDataToGoogleSheet($spreadsheetId, $list, $header,'Sheet1');

        return $spreadsheetId;

    }


    /**
     * @param $title
     * @param $data
     * @param $folderId
     * @return string
     * @throws Exception
     */
    public function exportConsolidateFollowingsByUsernameToSpreadsheet ($title,$data,$folderId) {

        // Export to sheets

        $spreadsheetId = $this->createFileInFolderIfNotExistOrReturnSpreadsheetId($folderId,'!!Project-'.$title);

        $header = array_merge(['Followed profile'],$this->generateSheetAccountsWithLocationsFirstRow());

        $list=[];
        foreach ($data as $username=>$accounts){
            foreach ($accounts as $account){
                array_push($list,array_merge([$username],$this->generateSheetAccountsWithLocationsDataRow($account)));
            }
        }

        $this->sendInstagramDataToGoogleSheet($spreadsheetId, $list, $header,'Sheet1');

        return $spreadsheetId;

    }

    /**
     * @param $folderId
     * @param $filename
     * @return string
     * @throws Exception
     */
    private function createFileInFolderIfNotExistOrReturnSpreadsheetId($folderId,$filename){
        try {

            $client = $this->getGoogleClient();

            $service = new \Google_Service_Drive($client);

            $optParams = array(
                'q' => "trashed = false AND '".$folderId."' in parents AND mimeType='application/vnd.google-apps.spreadsheet'"
            );

            $folder = $service->files->listFiles($optParams);

            $created = false;
            foreach ($folder->getFiles() as $elem){
                if ($elem->name == $filename) {
                    $targetFolderId = $elem->id;
                    return $targetFolderId;
                }
            }

            if (!$created) {
                $fileMetadata = new \Google_Service_Drive_DriveFile(array(
                    'name' => $filename,
                    'parents' => array($folderId),
                    'mimeType' => 'application/vnd.google-apps.spreadsheet' // указываем Mime-type файла
                ));

                $file = $service->files->create($fileMetadata, array('fields' => 'id')); // указываем что нужно вернуть нам $file->id
                return $file->id;
            }

        } catch (Exception $exception){
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @param $folderId
     * @param $list
     * @param $header
     * @param $filename
     * @return array
     * @throws Exception
     */
    private function sendInstagramDataToGoogleNewSpreadsheet($folderId, $list, $header, $filename){

        try {

            $client = $this->getGoogleClient();

            $service = new \Google_Service_Drive($client);

            $optParams = array(
                'pageSize' => 10,
                'fields' => "nextPageToken, files(contentHints/thumbnail,fileExtension,iconLink,id,name,size,thumbnailLink,webContentLink,webViewLink,mimeType,parents)",
                'q' => "'".$folderId."' in parents"
            );
            $folder = $service->files->listFiles($optParams);

            $created = false;
            $spreadsheetId = '';
            foreach ($folder->getFiles() as $elem){
                if ($elem->name == $filename) {
                    $created = true;
                    $spreadsheetId = $elem->id;
                }
            }

            if (!$created) {
                $fileMetadata = new \Google_Service_Drive_DriveFile(array(
                    'name' => $filename,
                    'parents' => array($folderId),
                    'mimeType' => 'application/vnd.google-apps.spreadsheet' // указываем Mime-type файла
                ));

                $file = $service->files->create($fileMetadata, array('fields' => 'id')); // указываем что нужно вернуть нам $file->id
                $spreadsheetId = $file->id;
            }

            $result = $this->sendInstagramDataToGoogleSheet($spreadsheetId,$list,$header);

        } catch (\Exception $exception){
            throw new Exception($exception->getMessage(),400);
        }

        return ['spreadsheetId'=>$spreadsheetId,'tabId'=>$result,'status'=>true];
    }

    /**
     * @param $spreadsheetId
     * @param $list
     * @param $header
     * @param string $targetSheet
     * @return bool
     * @throws Exception
     */
    private function sendInstagramDataToGoogleSheet($spreadsheetId, $list, $header, $targetSheet = 'Sheet1'){

        try {

            $values = [];
            $client = $this->getGoogleClient();
            $service = new Google_Service_Sheets($client);
            $range = $targetSheet.'!A1:'.chr(64+count($header));
            array_push($values, $header);
            foreach ($list as $element){
                array_push($values, $element);
            }

            $body = new \Google_Service_Sheets_ValueRange([
                    'values'=>$values,
                    'range'=>$range,
                    'majorDimension'=>'ROWS'
                ]
            );

            $service->spreadsheets_values->update(
                $spreadsheetId,
                $range,
                $body,
                ['valueInputOption' => 'USER_ENTERED']
            );

        } catch (\Exception $exception){
            throw new Exception($exception->getMessage(),400);
        }

        return true;
    }

    /**
     * @param $spreadsheetId
     * @param $title
     * @return bool
     * @throws Exception
     */
    private function createNewSheetToExistsSpreadsheet($spreadsheetId, $title){

        try {
            $client = $this->getGoogleClient();
            $service = new Google_Service_Sheets($client);

            $response = $service->spreadsheets->get($spreadsheetId);
            foreach($response->getSheets() as $sheet) {
                if ($sheet['properties']['title'] == $title){
                    return false;
                }
            }

            $body = new \Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
                'requests' =>
                    ['addSheet' =>
                        ['properties' =>
                            ['title' => $title]
                        ]
                    ]
                ]);

            $result = $service->spreadsheets->batchUpdate($spreadsheetId, $body);

        } catch (\Exception $exception){
            throw new Exception($exception->getMessage(),400);
        }

        return true;

    }


    /**
     * @return Google_Client
     * @throws \Exception
     */
    private function getGoogleClient() {

        $auth = file_get_contents(self::GoogleCredentials);

        try {
            $client = new Google_Client();
            $client->useApplicationDefaultCredentials();
            $client->setApplicationName('Assem2-export');
            $client->setScopes([
                \Google_Service_Sheets::SPREADSHEETS,
                \Google_Service_Drive::DRIVE
            ]);
            $client->setAccessType('offline');
            $client->setAuthConfig(json_decode($auth, true));
        } catch (\Google_Exception $exception){
            throw new \Exception(__FUNCTION__.' '.$exception->getMessage(),400);
        }

        return $client;
    }

    /**
     * @param $element
     * @return array
     */
    private function generateSheetInstagramListDataRow($element){
        return [
            #date('c',$element['updated']),
            !empty($element['username'])?$element['username']:'',
            !empty($element['business'])?$element['business']:'n/a',
            !empty($element['username'])?'=HYPERLINK("https://www.instagram.com/'.$element['username'].'";"Link")':'n/a',
            !empty($element['posts'])?$element['posts']:'n/a',
            !empty($element['follow'])?$element['follow']:'n/a',
            !empty($element['followers'])?$element['followers']:'n/a',
            !empty($element['er'])?$element['er']:'n/a',
            !empty($element['bio'])?$element['bio']:'n/a',
            !empty($element['user_id'])?$element['user_id']:'n/a',
            !empty($element['user_fake'])?$element['user_fake']:'0',
            !empty($element['user_real'])?$element['user_real']:'0',
            !empty($element['user_mass'])?$element['user_mass']:'0',
            !empty($element['user_influ'])?$element['user_influ']:'0',
            !empty($element['user_inact'])?$element['user_inact']:'0'
        ];
    }

    /**
     * @return array
     */
    private function generateSheetInstagramListFirstRow(){
        return [
            'Account',
            'Business',
            'URL',
            'Posts',
            'Follow',
            'Followers',
            'ER%',
            'Bio',
            'UserID',
            'Fake %',
            'Real %',
            'Mass %',
            'Influ %',
            'Inact %'
        ];
    }

    private function generateSheetAccountsWithLocationsDataRow($element){
        return [
            #date('c',$element['updated']),
            !empty($element['username'])?$element['username']:'',
            !empty($element['business'])?$element['business']:'n/a',
            !empty($element['username'])?'=HYPERLINK("https://www.instagram.com/'.$element['username'].'";"Link")':'n/a',
            !empty($element['posts'])?$element['posts']:'n/a',
            !empty($element['follow'])?$element['follow']:'n/a',
            !empty($element['followers'])?$element['followers']:'n/a',
            !empty($element['er'])?$element['er']:'n/a',
            !empty($element['bio'])?$element['bio']:'n/a',
            !empty($element['followers_stat']['real'])?$element['followers_stat']['real']:'0',
            !empty($element['followers_stat']['fake'])?$element['followers_stat']['fake']:'0',
            !empty($element['followers_stat']['influ'])?$element['followers_stat']['influ']:'0',
            !empty($element['followers_stat']['mass'])?$element['followers_stat']['mass']:'0',
            !empty($element['followers_stat']['inact'])?$element['followers_stat']['inact']:'0',
            !empty($element['user_id'])?$element['user_id']:'n/a',
            !empty($element['bio_email'])?$element['bio_email']:'n/a',
            !empty($element['public_email'])?$element['public_email']:'n/a',
            !empty($element['category'])?$element['category']:'n/a',
#            !empty($element['main_country'])?$element['main_country']:'',
#            !empty($element['main_percent'])?$element['main_percent']:'0',
            !empty($element['all_country'])?$element['all_country']:'',
        ];
    }

    private function generateSheetAccountsWithLocationsFirstRow(){
        return [
            'Account',
            'Business',
            'URL',
            'Posts',
            'Follow',
            'Followers',
            'ER%',
            'Bio',
            'Real',
            'Fake',
            'Influ',
            'Mass',
            'Inact',
            'UserID',
            'BioEmail',
            'PublicEmail',
            'Category',
            'All countries'
        ];
    }

}