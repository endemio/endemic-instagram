<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 18.02.2019
 * Time: 16:23
 */

namespace App\Service;


use App\Controller\MainController;
use App\Entity\Project;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Exception;

class ProjectService extends MainController
{

    public function listAllActiveProjects(int $type){

        $accounts = $this->getDoctrine()->getRepository(Project::class)->findAllActiveProjectsByType($type);

        return $accounts;
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function addNewProjectEntity(array $data){

        $ids = [];

        foreach ($data as $element) {
            try {
                $em = $this->getDoctrine()->getManager();

                $project = new Project();

                $project
                    ->setProjectType($element['project_type'])
                    ->setDetail($element['detail'])
                    ->setTitle($element['title']);

                $em->persist($project);

                $em->flush();

                $ids[] = $project->getId();

            } catch (UniqueConstraintViolationException $e) {

                $this->getDoctrine()->resetManager();

            } catch (\Exception $exception) {
                throw new Exception($exception->getMessage());
            }
        }

        return $ids;
    }

    /**
     * @param int $id
     * @param array $data
     * @throws Exception
     */
    public function updateProjectById(int $id, array $data){

        $em = $this->getDoctrine()->getManager();

        $account =$em->getRepository(Project::class)->find($id);

        try{
            $this->updateProjectEntity($account,$em,$data);
        } catch (Exception $exception){
            throw  new Exception($exception->getMessage());
        }
    }

    /**
     * @param Project $project
     * @param ObjectManager $em
     * @param array $data
     * @throws Exception
     */
    private function updateProjectEntity(Project $project, ObjectManager $em, array $data){
        $updated = false;
        foreach ($data as $el){
            switch ($el['name']){
                case 'updated':
                    $updated = true;
                    $project->setUpdated($el['value']);
                    break;
                case 'start':
                    $updated = true;
                    $project->setStart($el['value']);
                    break;
                case 'status':
                    $updated = true;
                    $project->setStatus($el['value']);
                    break;
            }
        }

        if ($updated) {
            try {
                $em->persist($project);

                $em->flush();

                $em->clear();
            } catch (Exception $exception) {
                throw new Exception('[' . __FUNCTION__ . '] ' . $exception->getMessage(), 400);
            }
        }
    }

    /**
     * @param $id
     * @return array
     * @throws Exception
     */
    public function getProjectDataById($id){

        try {
            $info = $this->getDoctrine()->getRepository(Project::class)->getInfoById($id);
        } catch (Exception $exception){
            throw new Exception($exception->getMessage());
        }

        return $info;
    }

    /**
     * @param $id
     * @return bool
     * @throws Exception
     */
    public function removeProjectById($id){

        try {
            $em = $this->getDoctrine()->getManager();

            $project = $em->getRepository(Project::class)->find($id);

            $em->remove($project);

            $em->flush();

            $em->clear();

            return true;
        } catch (Exception $exception){
            throw  new Exception($exception->getMessage());
        }
    }

    public function checkIfAccountFineToFullAudit($filter,$account,$countries){

        #print_r($filter['countries']['items']);

        #print_r($account);

        #print_r($countries);

        $status = false;

        foreach ($countries as $country=>$accuracy){

            #print '     Country '.$country.'  accuracy '.$accuracy.PHP_EOL;

            if (in_array($country,$filter['countries']['items'])){
                if ($accuracy >= $filter['countries']['accuracy']){
                    $status = true;
                }
            }
        }

        if (!empty($filter['followers']['followers_min'])){
            if ($filter['followers']['followers_min'] < $account['followers']){
                $status = true;
            }
        }

        if (!empty($filter['followers']['followers_max'])){
            if ($filter['followers']['followers_max'] > $account['followers']){
                $status = true;
            }
        }

        return $status;
    }

    public function getProjectListFromFileById($project_id,$account)
    {

        $filename = self::FolderStorage . '/projects/' . $project_id.'_'.$account. '.list';

        $result = [];
        if (is_file($filename)) {

            $result = explode(PHP_EOL, file_get_contents($filename));

            $result = array_unique($result);
        }

        return $result;
    }

    public function storeUsernameInProjectFile($project_id,$account,$username){

        $filename = self::FolderStorage.'/projects/'.$project_id.'_'.$account.'.list';

        #print '     Store username in project file '.$filename.PHP_EOL;

        file_put_contents($filename,$username.PHP_EOL,FILE_APPEND);

        return true;
    }

    public function removeUsernameInProjectFile($project_id){

        $filename = self::FolderStorage.'/projects/'.$project_id.'.list';

        if (is_file($filename)){
            unlink($filename);
        }

        return true;
    }
}