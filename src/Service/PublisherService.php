<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 17.02.2019
 * Time: 9:30
 */

namespace App\Service;

use App\Controller\MainController;
use Ramsey\Uuid\Uuid;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;

class PublisherService extends MainController
{
    private $rabbit;

    public function __construct(ProducerInterface $producer)
    {

        $this->rabbit = $producer;

    }

    public function exportDataToGoogleSheet(int $account_id, string $project_type, string $export_type)
    {

        $steps = [
            [
                'task' => 'export-data-to-google-sheet',
                "date" => date('c'),
            ],
        ];

        $message = [
            "project_flow" => $steps,
            "account_id" => $account_id,
            "project_type" => $project_type,
            "export_type" => $export_type
        ];

        $rabbitMessage = json_encode($message);

        $this->rabbit->publish($rabbitMessage, 'application', ['priority' => 8]);

        return $rabbitMessage;
    }

    public function exportConsolidateDataToGoogleSheet(int $project_id, string $project_type)
    {

        $steps = [
            [
                'task' => 'export-consolidate-data-to-google-sheet',
                "date" => date('c'),
            ],
        ];

        $message = [
            "project_flow" => $steps,
            "project_type" => $project_type,
            "token" => $project_id,
        ];

        $rabbitMessage = json_encode($message);

        $this->rabbit->publish($rabbitMessage, 'application', ['priority' => 8]);

        return $rabbitMessage;
    }

    
}