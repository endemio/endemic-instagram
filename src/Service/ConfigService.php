<?php
/**
 * Created by PhpStorm.
 * User: Дима
 * Date: 08.04.2019
 * Time: 16:01
 */

namespace App\Service;


use App\Controller\MainController;

class ConfigService extends MainController
{

    /**
     * @return array
     * @throws \Exception
     */
    public function getConfigDataFromFile(){
        try{
            if (is_file(self::AppConfig)) {
                $object = json_decode(file_get_contents(self::AppConfig),true);
            } else {
                $object = [];
            }
        } catch (\Exception $exception){
            throw  new \Exception($exception->getMessage());
        }

        return $object;
    }
}